YawikDemoJobboard
=================

This YAWIK Module makes YAWIK to work like a jobportal.

* The Startpage contains a List of Jobs
* Default role of users is "user"
* simple controller for an about page

This module is running on:

http://jobs.yawik.org

You can use this Module as starting point to write your own Module.
